﻿Feature: ExtCalculatorStandartFunctions
	As user
	I want to execute operations by calculator
	And calculator is in extended mode
	And get correct result


Scenario Outline: Two elements, operation and equals are present
	Given Calculator in Scientific mode
	When I enter <first-number>
	And I press <operation>
	And I enter <second-number>
	And I press Equals
	Then result on screen is <result>

Examples: 
| first-number | second-number | operation | result |
| 120          | 256           | Add       | 376    |
| 128          | 256           | Subtract  | -128   |


Scenario Outline: One element and one operation are present
	Given Calculator in Scientific mode
	When I enter <number>
	And I press <operation>
	Then result on screen is <result>

Examples:
| number | operation   | result |
| 4      | Square root | 2      |


Scenario Outline: Result of operation is displayed after press next operation button
	Given Calculator in Scientific mode
	When I enter <first-number>
	And I press <operation>
	And I enter <second-number>
	And I press <next-operation>
	Then result on screen is <result>

Examples:
| first-number | operation | second-number | next-operation | result |
| 1024         | Add       | 256           | Add            | 1280   |
| 128          | Multiply  | 2             | Multiply       | 256    |


Scenario Outline: Result of previous operations is displayed in history section
	Given Calculator in Scientific mode
	And Calculator in History mode
	When I enter <first-number>	
	And I press <operation>
	And I enter <second-number>
	And I press Equals
	Then <first-number> <operation> <second-number> are presents in History
Examples: 
| first-number | operation | second-number |
| 256          | Multiply  | 16            |


Scenario Outline: Calculator has radio buttons that able to switch
	Given Calculator in Scientific mode
	When I press <radio-button>
	Then <radio-button> is selected
Examples: 
| radio-button |
| Grads        |
| Degrees      |
| Radians      |


Scenario Outline: User Performs operations by Unit Convertion functional
	Given Calculator in Scientific mode
	And Calculator in Unit conversion mode
	When I select <unit-type> in Unit Type box section
	And I put <value> in "From Value" field
	And I select <from-unit> in From Unit box section
	And I select <to-unit> in To Unit box section
	Then result in "To Value" edit is <result>

Examples:
| unit-type | value | from-unit | to-unit      | result    |
| Time      |  2     | Day       | Millisecond | 172800000 |
