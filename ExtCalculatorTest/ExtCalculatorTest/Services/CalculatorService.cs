﻿using System;
using System.Collections.Generic;
using System.Text;
using ExtCalculatorTest.ExtCalculatorTest.Product;
using TestStack.White;
using ExtCalculatorTest.ExtCalculatorTest.Exceptions;
using ExtCalculatorTest.ExtCalculatorTest.Utils;

namespace ExtCalculatorTest.ExtCalculatorTest.Services
{
    class CalculatorService
    {
        private readonly Calculator _calculator = new Calculator();
        private const char Splitter = ' ';

        public readonly Dictionary<string, string> OperationSignsByName =
            new Dictionary<string, string>
            {
                { "Add", "+"},
                { "Subtract",  "-"},
                { "Divide",  "/"},
                { "Multiply", "*"},
                {"Square root", "sqrt"}
            };

        public void PressButton(string name)
        {
            try
            {
                _calculator.ClickButton(name);
            }
            catch (AutomationException e)
            {
                var exceptionMesage = "Button with name : \"" + name + "\" is not exists!\n" + e.StackTrace;
                Log.Info(exceptionMesage);
                throw new ButtonNotFoundException(exceptionMesage, e);
            }
        }

        public void EnterData(string sequence)
        {
            var targetSequence = sequence.ToCharArray();
            Array.ForEach(targetSequence, symbol => PressButton(symbol.ToString()));
        }

        public bool IsRadioButtonSelected(string name)
        {
            var targetButton = _calculator.GetRadioButton(name);
            return targetButton.IsSelected;
        }

        public void LaunchApp()
        {
            _calculator.LaunchApp();
        }

        public void CloseApp()
        {
            _calculator.CloseApp();
        }

        public void ClickMenuItems(string[] menuSelectionSequence)
        {
            _calculator.ClickMenuItems(menuSelectionSequence);
        }

        public bool IsResultOnMainScreenAsExpected(string expectedResult)
        {
            var actualResult = _calculator.GetResultFromMainScreen();
            return expectedResult.Equals(actualResult);
        }

        public void EnterValueToTextBox(string value, string fieldName)
        {
            _calculator.EnterValueToTextBox(value, fieldName);
        }

        public void SelectValueInComboBox(string comboBoxName, string targetSelection)
        {
            _calculator.SelectValueInComboBox(comboBoxName, targetSelection);
        }

        public bool IsResultFromFieldAsExpected(string fieldName, string expectedResult)
        {
            var actualResult = _calculator.GetResultFromTextBox(fieldName);
            return expectedResult.Equals(actualResult);
        }

        public bool IsResultInHistoryAsExpected(string expectedData)
        {
            var actualData = _calculator.GetResultFromHistory();
            return actualData.Contains(ConvertDataFromHistory(expectedData));
        }

        private string ConvertDataFromHistory(string data)
        {
            var operations = data.Split(Splitter);
            var newOperations = new StringBuilder();
            foreach (var element in operations)
            {
                var newElement = element;
                if (OperationSignsByName.ContainsKey(element))
                {
                    newElement = Splitter + OperationSignsByName[element] + Splitter;
                }
                newOperations.Append(newElement);
            }
            return newOperations.ToString();
        }
    }
}
