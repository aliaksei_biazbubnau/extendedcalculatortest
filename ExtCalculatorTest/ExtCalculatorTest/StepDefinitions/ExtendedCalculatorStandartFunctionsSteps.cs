﻿using TechTalk.SpecFlow;
using ExtCalculatorTest.ExtCalculatorTest.Services;
using NUnit.Framework;
using ExtCalculatorTest.ExtCalculatorTest.Exceptions;

namespace ExtCalculatorTest.ExtCalculatorTest.StepDefinitions
{
    [Binding]
    public sealed class ExtendedCalculatorStandartFunctionsSteps
    {
        private readonly CalculatorService _service = new CalculatorService();
        private const string ViewMenuName = "View";

        [Before]
        public void SetUp()
        {
            _service.LaunchApp();
        }

        [After]
        public void TearDown()
        {
            _service.CloseApp();
        }
        
        [Given(@"Calculator in (Scientific|History|Unit conversion|Date calculation) mode")]
        public void GivenCalculatorIsInMode(string mode)
        {  
            string[] menuSelectionSequence = { ViewMenuName, mode };
            _service.ClickMenuItems(menuSelectionSequence);
        }
        
        [When(@"I enter (.*)")]
        public void WhenIEnterSequence(string sequence)
        {
           try {
                _service.EnterData(sequence);
            } catch (ButtonNotFoundException e)
            {
                Assert.Fail(e.Message);
            } 
        }

        [When(@"I press (.*)")]
        public void WhenIpressButton(string buttonName)
        {                
            try
            {
                _service.PressButton(buttonName);
            }
            catch (ButtonNotFoundException e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Then(@"result on screen is (.*)")]
        public void ThenTheResultOnScreenIs(string expectedResult)
        {
            var result = _service.IsResultOnMainScreenAsExpected(expectedResult);
            Assert.True(result, "Result in screen main section should be same as expected!");
        }

        [Then(@"(.*) are presents in History")]
        public void DataDisplayedInSection(string expectedData)
        {
            var result = _service.IsResultInHistoryAsExpected(expectedData);
            Assert.True(result, "Every element in this section should be present in expected section!");
        }
        [Then(@"(.*) is selected")]
        public void ThenElementShouldBeActive(string elementName)
        {
            var result = _service.IsRadioButtonSelected(elementName);
            Assert.True(result, "RadioButton should be enabled!");
        }

        [When(@"I select (.*) in (Unit Type|From Unit|To Unit|Date Time) box section")]
        public void WhenISelectTimeInUnitTypeSection(string selection, string comboBoxName)
        {
            _service.SelectValueInComboBox(comboBoxName, selection);
        }

        [When(@"I put (.*) in ""(.*)"" (?:field)")]
        public void WhenIEnterValueToField(string sequence, string targetFieldName)
        {
            _service.EnterValueToTextBox(sequence, targetFieldName);
        }

        [Then(@"result in ""(.*)"" edit is (.*)")]
        public void ThenResultInEditBoxIs(string fieldName, string expectedResult)
        {
            var result = _service.IsResultFromFieldAsExpected(fieldName, expectedResult);
            Assert.True( result, "Both results: Expected from scenario and Actual from application should be match!");
        }
    }
}
