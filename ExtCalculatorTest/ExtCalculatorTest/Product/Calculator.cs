﻿using System;
using System.Collections.Generic;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using ExtCalculatorTest.ExtCalculatorTest.Utils;
using TestStack.White.UIItems.ListBoxItems;
using ExtCalculatorTest.ExtCalculatorTest.Exceptions;
using static ExtCalculatorTest.ExtCalculatorTest.Product.CalculatorElements;

namespace ExtCalculatorTest.ExtCalculatorTest.Product
{
    class Calculator
    {
        private Application _calculator;
        private Window _calculatorWindow;
        private const string PathToApplication = @"C:\Windows\System32\calc.exe";
        private const string CalculatorWindowName = "Calculator";
        private const string ResultTextBoxId = "150";
        private const string LogItemsSeparator = "-> ";
        private const string HistoryListId = "153";

        public void LaunchApp()
        {
            Log.Info("Starting Application...");
            _calculator = Application.Launch(PathToApplication);
            _calculatorWindow = _calculator.GetWindow(CalculatorWindowName);
            Log.Debug("Application has been Started.");
        }

        public void CloseApp()
        {
            Log.Info("Closing Application...");
            _calculator.Close();
            Log.Info("Application has been Closed.");
        }

        public void ClickButton(string name)
        {
            Log.Info("Clicking on button: " + name);
            var targetButton = _calculatorWindow.Get(SearchCriteria.ByText(name));
            try
            {
                targetButton.Click();
            } catch (AutomationException e)
            {
                var message = "Can't find button with name: " + name;
                Log.Info(message);
                throw new ButtonNotFoundException(message, e);
            }
        }

        public void EnterValueToTextBox(string sequence, string targetFieldName)
        {
            Log.Info("Enter data: " + sequence + " to the text box with name: " + targetFieldName);
            var targetBox = _calculatorWindow.Get<TextBox>(SearchCriteria.ByAutomationId(EditFieldsIdByName[targetFieldName]));
            targetBox.Text = sequence;
        }

        public void ClickMenuItems(string[] menuPath)
        {
            var menuPathDefinitionToLog = String.Join(LogItemsSeparator, menuPath);
            Log.Info("Clicking on menu items:" + menuPathDefinitionToLog);
            var menuItem = _calculatorWindow.MenuBar.MenuItem(menuPath);
            menuItem.Click();
        }

        public void SelectValueInComboBox(string comboBoxName, string targetSelection)
        {
            Log.Info("Selected: " + targetSelection + " from Combo Box with name: " + comboBoxName);
            var currentBox = _calculatorWindow.Get<ComboBox>(SearchCriteria.ByAutomationId(ComboBoxesIdByName[comboBoxName]));
            currentBox.Click();
            currentBox.Select(targetSelection);
        }

        public string GetResultFromMainScreen()
        {
            Log.Info("Getting Result value...");
            var resultLable = _calculatorWindow.Get<Label>(SearchCriteria.ByAutomationId(ResultTextBoxId));
            var resultText = resultLable.Text;
            Log.Debug("Result value is: " + resultText);
            return resultText;
        }

        public List<string> GetResultFromHistory()
        {
            Log.Info("Getting Result from History section");
            var sectionList = _calculatorWindow.Get<ListBox>(SearchCriteria.ByAutomationId(HistoryListId));
            var historyElements = new List<string>();
            sectionList.Items.ForEach(historyElement => historyElements.Add(historyElement.Text));
            return historyElements;
        }

        public string GetResultFromTextBox(string fieldName)
        {
            Log.Info("Getting result from: " + fieldName + " Text Box");
            var targetBox = _calculatorWindow.Get<TextBox>(SearchCriteria.ByAutomationId(EditFieldsIdByName[fieldName]));
            return targetBox.Text;
        }

        public RadioButton GetRadioButton(string name)
        {
            Log.Info("Getting Radio Button with name: " + name);
            return _calculatorWindow.Get<RadioButton>(SearchCriteria.ByText(name));
        }
    }
}
