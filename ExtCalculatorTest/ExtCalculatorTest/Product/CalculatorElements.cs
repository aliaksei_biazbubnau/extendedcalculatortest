﻿using System.Collections.Generic;

namespace ExtCalculatorTest.ExtCalculatorTest.Product
{
    static class CalculatorElements
    {

        public static readonly Dictionary<string, string> ComboBoxesIdByName =
            new Dictionary<string, string>
            {
                {"Unit Type", "221"},
                {"From Unit", "224"},
                {"To Unit", "225"},
                {"Date Time", "4003"}
            };

        public static readonly Dictionary<string, string> EditFieldsIdByName =
            new Dictionary<string, string>
            {
                {"From Value", "226" },
                {"To Value", "227" },
                {"Year(s)", "4010" },
                { "Month(s)","4011"},
                { "Day(s)","4012"}
            };
    }
}
