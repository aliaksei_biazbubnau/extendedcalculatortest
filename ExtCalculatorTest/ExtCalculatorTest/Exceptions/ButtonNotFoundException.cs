﻿using System;

namespace ExtCalculatorTest.ExtCalculatorTest.Exceptions
{
    class ButtonNotFoundException : Exception
    {
        public ButtonNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
